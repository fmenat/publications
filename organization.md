# List of Publications
---

|Name|Keywords|Publication|Presentation|Poster|Code|
|---|---|---|---|---|---|
|On the Quality of Deep Representations for Kepler Light Curves Using Variational Auto-Encoders| *Variational Auto-Encoder · Representation Learning · Transit Model · Light-Curve · Unsupervised Learning* | [:heavy_check_mark:](./manuscripts/2021_Signals_VAE.pdf)| :x: | [:heavy_check_mark:](./posters/2020_VAE.pdf) | __[github](https://github.com/Buguemar/PIIC19/tree/master/code/obj4)__ 
|Self-Supervised Bernoulli Autoencoders for Semi-Supervised Hashing| *Variational Autoencoders· Hashing · Self-Supervision · Semi-Supervision · Deep Learning* | [:heavy_check_mark:](./manuscripts/Preprint_SelfB-VAE.pdf)| [:heavy_check_mark:](./presentations/2021_CIARP_SSBVAE.pdf) | :x: | __[github](https://github.com/amacaluso/SSB-VAE)__ |
|Harnessing the power of CNNs for unevenly-sampled light-curves using Markov Transition Field| *Markov Transition Field · Light Curves · Exoplanet Identification · Convolutional Neural Networks · Unevenly-Sampled Light Curves* | [:heavy_check_mark:](./manuscripts/2021_AC_MTF.pdf)| :x: | [:heavy_check_mark:](./posters/2020_MTF.pdf) | __[github](https://github.com/Buguemar/PIIC19/tree/master/code/obj1)__ |
|Interpretable and effective hashing via Bernoulli variational auto-encoders| *Hashing · Variational Autoencoders · Deep Learning · Gumbel-Softmax Distribution · Neural Information Retrieval* | [:heavy_check_mark:](./manuscripts/2020_IDA_BVAE.pdf)| :x: | :x: | __[github](https://github.com/FMena14/DiscreteVAE)__ |
|Collective annotation patterns in learning from crowds| *Learning from Crowds · Mixture Model · Multiple Annotations · Clustering* | [:heavy_check_mark:](./manuscripts/2020_IDA_CMM.pdf)| :x: | :x:| __[github](https://github.com/FMena14/PyLearningCrowds)__ |
|Can we interpret machine learning? An analysis of exoplanet detection problem| *Machine Learning · Exoplanet* | [:heavy_check_mark:](./manuscripts/2019_ADASS_Exoplanet.pdf)| :x: | [:heavy_check_mark:](./posters/2019_ADASS_Exoplanet.pdf) | -|
|Classical machine learning techniques in the search of extrasolar planets| *Machine Learning · Exoplanet Detection · Feature Engineering · Light-curve* | [:heavy_check_mark:](./manuscripts/2019_CLEIEJ_Exoplanet.pdf)| :x: | :x: |  __[github](https://github.com/FMena14/ExoplanetDetection)__ |
|Revisiting machine learning from crowds a mixture model for grouping annotations| *Learning from Crowds · Mixture Model · Multiple Annotations · Clustering* | [:heavy_check_mark:](./manuscripts/2019_CIARP_CMM.pdf)| [:heavy_check_mark:](./presentations/2019_CIARP_CMM.pdf) | :x: | __[github](https://github.com/FMena14/MixtureofGroups)__ |
|A binary variational autoencoder for hashing| *Hashing · Variational Autoencoders · Deep Learning · Gumbel-Softmax Distribution · Neural Information Retrieval.* | [:heavy_check_mark:](./manuscripts/2019_CIARP_BVAE.pdf)| [:heavy_check_mark:](./presentations/2019_CIARP_BVAE.pdf) | :x: | __[github](https://github.com/FMena14/DiscreteVAE)__ |
|Refining exoplanet detection using supervised learning and feature engineering| *Machine Learning · Exoplanet Detection · Feature Engineering* | [:heavy_check_mark:](./manuscripts/2018_CLEI_Exoplanet.pdf)| [:heavy_check_mark:](./presentations/2018_SLIOA-CLEI_Exoplanet.pdf) | [:heavy_check_mark:](./posters/2018_ChileWIC_exoplanet.pdf) |  __[github](https://github.com/FMena14/ExoplanetDetection)__ |



[UP](#list-of-publications)

__[Home](./README.md)__
